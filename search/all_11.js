var searchData=
[
  ['sawwave_0',['SawWave',['../main1_8py.html#aa925b60aeb90ef2d5223ca138515a472',1,'main1']]],
  ['set_5fduty_1',['set_duty',['../classmotor_1_1_motor.html#a08f41a32c8b122a8b94496fef2a0a901',1,'motor.Motor.set_duty(self, duty)'],['../classmotor_1_1_motor.html#a08f41a32c8b122a8b94496fef2a0a901',1,'motor.Motor.set_duty(self, duty)'],['../classmotor_1_1_motor.html#a08f41a32c8b122a8b94496fef2a0a901',1,'motor.Motor.set_duty(self, duty)']]],
  ['setkd_2',['setKd',['../classclosed_loop_1_1_closed_loop.html#aeac543f2449d19d7becb9d549c584f47',1,'closedLoop::ClosedLoop']]],
  ['setkp_3',['setKp',['../classclosed_loop_1_1_closed_loop.html#adb7f9c280444acabd058f3f590cbe84b',1,'closedLoop.ClosedLoop.setKp(self, kProp)'],['../classclosed_loop_1_1_closed_loop.html#adb7f9c280444acabd058f3f590cbe84b',1,'closedLoop.ClosedLoop.setKp(self, kProp)']]],
  ['sflag_4',['sFlag',['../main2_8py.html#a3c7f44e82daf9b9be827139513f4f3ae',1,'main2.sFlag()'],['../main3_8py.html#ac46ab7773e48c113fad79b85fdab0bda',1,'main3.sFlag()'],['../main4_8py.html#a82ba8ee0908c79c639a0ab04796a3320',1,'main4.sFlag()'],['../main6_8py.html#a2229ec5f36b9fb7e8f0c44a966ba6b40',1,'main6.sFlag()']]],
  ['share_5',['Share',['../classshares_1_1_share.html',1,'shares']]],
  ['shares_2epy_6',['shares.py',['../_lab2_2shares_8py.html',1,'(Global Namespace)'],['../_lab3_2shares_8py.html',1,'(Global Namespace)'],['../_lab4_2shares_8py.html',1,'(Global Namespace)'],['../_term_project_2shares_8py.html',1,'(Global Namespace)']]],
  ['sinewave_7',['SineWave',['../main1_8py.html#aca8e70e12834fea178ab03d41d3f1d33',1,'main1']]],
  ['squarewave_8',['SquareWave',['../main1_8py.html#af8fa9e098d6321746949d5a4107fb429',1,'main1']]],
  ['starttime_9',['StartTime',['../main1_8py.html#a53646f5f99c2cdd7f05017ab941f6563',1,'main1']]],
  ['state_10',['state',['../main1_8py.html#a269451f4246b75d066e8a94246af5f89',1,'main1']]]
];
